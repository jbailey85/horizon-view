variable "vsphere_server" {
  description = "vCenter Server FQDN"
  type        = string
}

variable "vsphere_user" {
  description = "vSphere Username"
  type        = string
}

variable "vsphere_password" {
  description = "vSphere Password"
  type        = string
  sensitive   = true
}

variable "datacenter" {
  description = "vSphere Datacenter"
  type        = string
}

variable "cluster" {
  description = "vSphere Cluster"
  type        = string
}

variable "datastore" {
  description = "vSphere Datastore"
  type        = string
}

variable "network_name1" {
  description = "VM Port Group Internet Facing"
  type        = string
}

variable "network_name2" {
  description = "VM Port Group Management"
  type        = string
}

variable "network_name3" {
  description = "VM Port Group Backend Horizon services"
  type        = string
}

variable "ipMode0" {
  description = "STATICV4"
  type        = string
}

variable "ipMode1" {
  description = "STATICV4"
  type        = string
}

variable "ipMode2" {
  description = "STATICV4"
  type        = string
}

variable "dnsSearch" {
  description = "DNS Search domains, e.g. domain.local"
  type        = string
}

variable "netmask0" {
  description = "Netmask for IP0"
  type        = string
}

variable "netmask1" {
  description = "Netmask for IP1"
  type        = string
}

variable "netmask2" {
  description = "Netmask for IP2"
  type        = string
}

variable "DNS" {
  description = "DNS servers"
  type        = string
}

variable "defaultGateway" {
  description = "Default gateway IP"
  type        = string
}

variable "disk_provisioning" {
  description = "Thick, Thin, etc."
  type        = string
}

variable "local_ovf_path" {
  description = "Path to local ovf/ova file"
  type        = string
}

variable "deployment_option" {
  description = "NIC amount and Size"
  type        = string
}

variable "adminPassword" {
  description = "Password for the UI Admin account"
  type        = string
  sensitive   = true
}

variable "rootPassword" {
  description = "Password for the OS root account"
  type        = string
  sensitive   = true
}

variable "deployment_host" {
  description = "Deploy OVF to this host"
  type        = string
}

variable "uag_prefix_ip0" {
  description = "Prefix IP for UAG NIC1 (eth0)"
  type        = string
}

variable "uag_start_address0" {
  description = "Starting Address for UAG (eth0)"
  type        = number
}

variable "uag_prefix_ip1" {
  description = "Prefix IP for UAG NIC2(eth1)"
  type        = string
}

variable "uag_start_address1" {
  description = "Starting Address for UAG NIC2 (eth1)"
  type        = number
}

variable "uag_prefix_ip2" {
  description = "Prefix IP for UAG NIC3 (eth2)"
  type        = string
}

variable "uag_start_address2" {
  description = "Starting Address for UAG NIC3 (eth2)"
  type        = number
}

variable "uag_name_prefix" {
  description = "UAG Name Prefix"
  type        = string
}

variable "uag_number" {
  description = "Amount of UAGs to deploy"
  type        = string
}

variable "routes1" {
  description = "Routes for ip1"
  type        = string
}

variable "routes2" {
  description = "Routes for ip2"
  type        = string
}

variable "horizon_folder" {
  description = "Folder name for Horizon VMs"
  type        = string
}