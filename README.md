# Horizon View Automation

This repository contains automation scripts for setting up and managing VMware Horizon View environments. The automation process includes deploying VMware virtual machines, configuring Horizon View roles, and setting up AppVolumes using Terraform and Ansible.

## Overview

The repository contains the following scripts:

1. `setup.sh`: Prepares the system by updating packages and installing required tools.
2. `deploy`: Deploys the Horizon View environment using Terraform and configures it with Ansible.
3. `uagdeploy`: Deploys UAG instances.

## Prerequisites

Before running the scripts, ensure the following prerequisites are met:

- Git is installed to clone the repository.
- Installation files for the Connection Server and AppVolumes are prepped on a file server.
- A Windows Server template is available in vCenter.
- You have the following credentials:
  - vSphere Administrator
  - Instant Clone Domain Admin
  - Server Template Admin
  - Horizon Events Database User Password (and server details)

## Getting Started

### Step 1: Clone this Repository

Clone the Horizon View automation repository:

```bash
git clone https://gitlab.com/jbailey85/horizon-view.git
cd horizon-view
```

Make the setup.sh script executable and run it. This script updates the system, installs various packages, including Terraform, Packer, and Ansible.

### Step 2: Run the Setup Script

```bash
chmod +x setup.sh
sudo ./setup.sh
```

### Step 3: Modify the variables.env File

```bash
vim variables.env
```

### Step 4: Run the Deploy Script

Execute the deploy script to start the Horizon View environment deployment. This script uses Terraform to create VMware VM instances defined in the variables.env file. After the instances are created, an Ansible playbook will run, configuring each instance for their respective Horizon View roles (Connection Server, Connection Server Replica(s), and AppVolumes).

```bash
./deploy
```

### Step 5: Run the UAG Deploy Script

Execute the uagdeploy script to start the Unified Access Gateway appliance deployment. This script uses a Terraform and a local ova file to create VMware VM instances defined in the variables.env file under the UAG Configuration section. You will be prompted to enter existing credentials to access vSphere for VM deployment, as well as credentials to be set for the Admin UI user, and root os user.

```bash
./uagdeploy
```
