#!/bin/bash

PACKER_VERSION=1.8.5
TERRAFORM_VERSION=1.3.7

# Detect CPU architecture
cpu_arch=$(uname -m)

# Set a variable based on the CPU architecture
case "$cpu_arch" in
    x86_64)
        arch_var="linux_amd64"
        ;;
    arm*)
        arch_var="linux_arm"
        ;;
    aarch64)
        arch_var="linux_arm64"
        ;;
    *)
        arch_var="unknown"
        ;;
esac

# Prompt the user for domain details
read -p "Enter the domain (e.g., example.com): " domain
read -p "Enter the domain controllers (comma-separated, e.g., dc1.example.com,dc2.example.com): " domain_controllers

# Convert domain to uppercase for realm
realm=$(echo "$domain" | tr '[:lower:]' '[:upper:]')

# Create a temporary krb5.conf file
tmp_krb5_conf=$(mktemp)

# Write the updated configuration to the temporary file
cat >"$tmp_krb5_conf" <<EOL
[libdefaults]
    default_realm = $realm
    dns_lookup_realm = false
    dns_lookup_kdc = false
    ticket_lifetime = 24h
    renew_lifetime = 7d
    forwardable = true
    rdns = false

[realms]
    $realm = {
$(echo "$domain_controllers" | sed 's/,/\n/g' | awk '{print "        kdc = "$0}')
        admin_server = $(echo "$domain_controllers" | cut -d',' -f1)
    }

[domain_realm]
    .$domain = $realm
    $domain = $realm
EOL

# Backup the existing krb5.conf file
sudo cp /etc/krb5.conf /etc/krb5.conf.backup

# Replace the krb5.conf file with the updated configuration
sudo mv "$tmp_krb5_conf" /etc/krb5.conf

# Set the proper permissions for the krb5.conf file
sudo chmod 644 /etc/krb5.conf

# Print a success message
echo "krb5.conf has been updated successfully."

# ----------------
# Install Packages
# ----------------

echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
apt update && apt upgrade -y

# Basic tools install and config
apt install -y tree vim ssh unzip sshpass neofetch

#Networking tools
apt install -y net-tools iproute2 iputils-ping iputils-tracepath traceroute nmap dnsutils mkisofs

#Web tools
apt install -y curl wget git

#Ansible
apt install -y python3-pip
pip3 install ansible
pip3 install pywinrm
pip3 install jinja2-cli

#Kerberos
apt-get install -y python-dev libkrb5-dev libpam-krb5 libpam-ccreds auth-client-config
apt-get install -y krb5-user
pip3 install pywinrm[kerberos]

#Download packer
cd packer
wget https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_${arch_var}.zip

#Unzip packer download
unzip packer_${PACKER_VERSION}_${arch_var}.zip

#move Packer binary to /usr/local/bin directory
mv packer /usr/local/bin

#remove packer zip file
rm packer_${PACKER_VERSION}_${arch_var}.zip

#download terraform
wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_${arch_var}.zip

#unzip terraform download
unzip terraform_${TERRAFORM_VERSION}_${arch_var}.zip

#move terraform binary to /usr/local/bin directory
mv terraform /usr/local/bin

#remove terraform zip file
rm terraform_${TERRAFORM_VERSION}_${arch_var}.zip

cd ..

#Make deploy and destroy scripts executable
chmod +x deploy
chmod +x destroy